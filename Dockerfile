FROM eclipse-temurin:21

ENV COMMANDLINE_TOOLS_VERSION 8512546_latest

ENV LANG en_US.UTF-8
ENV TERM xterm-256color

ENV ANDROID_SDK_ROOT /opt/android-sdk-linux
ENV PATH $PATH:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin:${ANDROID_SDK_ROOT}/cmdline-tools/bootstrap/bin

WORKDIR /opt/android-sdk-linux

RUN apt update -y && apt upgrade -y
RUN apt install -y software-properties-common build-essential git unzip
RUN apt autoremove -y

RUN wget https://dl.google.com/android/repository/commandlinetools-linux-${COMMANDLINE_TOOLS_VERSION}.zip
RUN wget --quiet https://dl.google.com/android/repository/commandlinetools-linux-${COMMANDLINE_TOOLS_VERSION}.zip -O /tmp/tools.zip && \
    mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools && \
    unzip -q /tmp/tools.zip -d ${ANDROID_SDK_ROOT}/cmdline-tools && \
    mv ${ANDROID_SDK_ROOT}/cmdline-tools/cmdline-tools ${ANDROID_SDK_ROOT}/cmdline-tools/bootstrap && \
    rm -v /tmp/tools.zip && \
    mkdir -p /root/.android/ && touch /root/.android/repositories.cfg

RUN yes | sdkmanager --licenses && \
    sdkmanager --update && \
    sdkmanager --list
RUN sdkmanager --install \
        "add-ons;addon-google_apis-google-24" \
        "build-tools;31.0.0" \
        "build-tools;32.0.0" \
        "build-tools;33.0.0" \
        "build-tools;34.0.0" \
        "cmake;3.22.1" \
        "cmdline-tools;latest" \
        "extras;android;m2repository" \
        "extras;google;google_play_services" \
        "extras;google;m2repository" \
        "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2" \
        "extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2" \
        "ndk;26.2.11394342" \
        "platform-tools" \
        "platforms;android-31" \
        "platforms;android-32" \
        "platforms;android-33" \
        "platforms;android-34"
RUN yes | sdkmanager --licenses
